# coding: utf-8
from __future__ import print_function
import os
import sys
import numpy as np


def epochs_cleaner(epochs, artifacts,
                   channels_to_care = tuple(),
                   head_min_overlap = 0., tail_min_overlap = 0.):
    """Rejects epochs containing artifacts
    :param epochs: list of obci epochs
    :param artifacts: list of (obci) tags describing artifacts
    :param channels_to_care: list of channel numbers (not names!) to care for (artifacts in other channels will not be considered), if empty (default) all  channels are considered
    :param head_min_overlap: minimal overlap [s] of epoch and artifact on the begging of an epoch
    :param tail_min_overlap: minimal overlap [s] of epoch and artifact on the end of an epoch
    :return: new list of epochs containing only artifacts free epochs
    """
    
    # tablica granic wszystkich artefaktów bez względu na kanał:
    artifacts_borders = np.array(
        [[np.inf, -np.inf]] +
        [[tag['start_timestamp'], tag['end_timestamp']] for tag in artifacts if (not channels_to_care or int(tag['channelNumber']) in channels_to_care)])

    # pętla po odcinkach z bodźcami i zapisywanie tylko tych, w których nie wystąpił żaden artefakt:
    clean_epochs = {}
    for idx, epoch in enumerate(epochs):
        # początek i koniec odcinka, który badamy
        start = float(epoch.get_start_timestamp()) + head_min_overlap
        end = float(epoch.get_end_timestamp()) - tail_min_overlap

        artifacts_starting_before_end = artifacts_borders[:, 0] < end
        artifacts_ending_after_start = artifacts_borders[:, 1] > start
        
        artifacts_overlapping = artifacts_starting_before_end * artifacts_ending_after_start
        
        if not np.any(artifacts_overlapping):
            clean_epochs[idx] = epoch
    
    print("Clean epochs: {} of {} stimulus".format(len(clean_epochs), len(epochs)))
    
    return clean_epochs


if __name__ == "__main__":
    import getpass
    from .obci.analysis.obci_signal_processing import read_manager
    from .obci.analysis.obci_signal_processing.tags.tags_file_reader import TagsFileReader

    from .helper_functions import get_microvolt_samples, montage_ears, get_epochs_from_rm
    
    path_prefix = ''
    if sys.argv[1:]:
        datapaths = sys.argv[1:]
    else:
        if getpass.getuser() == 'marcin':
            path_prefix = '/run/user/1000/gvfs/sftp:host=gabor.zfb.fuw.edu.pl,user=mpietrzak'
            if not os.path.exists(path_prefix):
                path_prefix = "/media/dane/Dokumenty/repo_mirror"
        else:
            path_prefix = ''
        
        datapaths = ["/repo/coma/dane_pomiarowe_dzienne"]
    
    selected_channels = [u'Fp1', u'Fp2', u'AFz', u'F7', u'F3', u'Fz', u'F4', u'F8', u'T7', u'C3', u'Cz',
                         u'C4', u'T8', u'P7', u'P3', u'Pz', u'P4', u'P8', u'O1', u'O2', u'T3', u'T4', u'T5', u'T6']
    
    for datapath in datapaths:
        for root, dirs, files in os.walk(path_prefix + datapath):
            for filename in files:  # przeglądamy rekursywnie wszystkie pliki w katalogu
                # sprawdzamy, czy sciezka do pliku i nazwa pliku pasuje do wzoru:
                if "global-local" not in root or "control" in root or "-bak" in root or ".raw" not in filename or ".etr." in filename:
                    continue
                print(filename)
                path_to_raw = os.path.join(root, filename)
                
                print(path_to_raw)
                core_of_path = path_to_raw[:-4]
                
                print("reading file...")
                eeg_rm = read_manager.ReadManager(core_of_path + '.xml', core_of_path + '.raw', core_of_path + '.tag')
                
                channels_names = eeg_rm.get_param('channels_names')
                
                print("preprocessing...")
                fs = float(eeg_rm.get_param('sampling_frequency'))
                
                eeg_rm.set_samples(get_microvolt_samples(eeg_rm), eeg_rm.get_param('channels_names'))
                eeg_rm.set_param('channels_gains', np.ones(len(channels_names)))
                
                unmountable_channels = [u'EOGL', u'EOGR', u'EMG', u'EKG', u'RESP', u'RESP_UP', u'RESP_U', u'RESP_DOWN', u'RESP_D', u'TERM', u'SaO2', u'Pleth', u'HRate', u'Saw', u'Driver_Saw', u'Sample_Counter']
                try:
                    x_ref = montage_ears(eeg_rm, 'A1', 'A2', exclude_from_montage = unmountable_channels)
                except ValueError:
                    try:
                        x_ref = montage_ears(eeg_rm, 'M1', 'M2', exclude_from_montage = unmountable_channels)
                    except ValueError:
                        print("WARNING! Cannot mount to ears for file {}".format(filename))
                        continue
                eeg_rm = x_ref
                
                available_channels = eeg_rm.get_param('channels_names')
                chan_2be_used = [ch for ch in available_channels if ch in selected_channels]

                tag_filters = None  # should be list of functions if someone want to filter
                epochs_list = get_epochs_from_rm(eeg_rm, tag_filters, start_offset = -0.1, duration = 2)  # lista epok dla bieżącego pliku

                print("rejecting epochs with artifacts...")
                art_tagfile_path = core_of_path.rstrip(".obci") + '_artifacts.obci.tag'
                print(art_tagfile_path)

                reader = TagsFileReader(art_tagfile_path)
                artifact_tags = reader.get_tags()

                clean_ep = epochs_cleaner(epochs_list, artifact_tags)
