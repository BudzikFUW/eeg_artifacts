#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Parametric Detection of EEG Artifacts
# Marcin Pietrzak 2017-03-01
# based on Klekowicz et al. DOI 10.1007/s12021-009-9045-2
from __future__ import print_function
import os
import sys

from copy import deepcopy

import numpy as np
from scipy import signal
import matplotlib.pyplot as plt

from .obci.analysis.obci_signal_processing.tags.tags_file_writer import TagsFileWriter

from .helper_functions import mgr_filter, get_microvolt_samples
from .helper_functions import map1020, channels_not2draw, set_axis_fontsize

from .helper_functions import figure_scale, fontsize

#  progi odnoszące się do std lub mediany są szacowane dla każdego kanału osobno
#  tagi są zaznaczane wg najostrzejszego z kryteriów
#  żeby nie korzystać z danego kryterium wystarczy podać wartość np.inf
#  każdy tag ma zapisane informacje:
#   - które kryterium było wykorzystane (typ:'abs', 'std' lub 'med')
#   - jaki był ostateczny próg (thr:wartość) w µV
#   - jaka wartość przekroczyła próg (val:wartość) w µV

DEBUG = False
DEBUG_FS = 128.

DefaultArtRejDict = {
    'SlopeWindow': 0.0704,  # [s]  # szerokość okna, w którym szukamy iglic i stromych zboczy (slope)
    'SlowWindow': 0.5,  # [s]  # szerokość okna, w którym szukamy fal wolnych (slow)
    'NoSigWindow': 0.125,  # [s]  # szerokość okna, w którym szukamy miejsc bez sygnału (nosig)
    
    'OutliersMergeWin': .1,  # [s] #szerokość okna, w którym łączymy sąsiadujące obszary z próbkami outlier
    
    'MusclesFreqRange': [40, 90],  # [Hz] przedział częstości, w którym szukamy artefaktów mięśniowych
    
    'SlopesAbsThr': 150,  # [µV] maksymalna bezwzględna wartość amplitudy peak2peak w oknie SlopeWindow
    'SlopesStdThr': 7,  # wartość peak2peak w oknie, jako wielokrotność std
    'SlopesMedThr': 6,  # wartość peak2peak w oknie, jako wielokrotność mediany
    
    'SlowsAbsThr': 250,  # [µV] maksymalna bezwzględna wartość amplitudy peak2peak w oknie SlowWindow
    'SlowsStdThr': 6,  # wartość peak2peak w oknie, jako wielokrotność std
    'SlowsMedThr': 4,  # wartość peak2peak w oknie, jako wielokrotność mediany
    
    'OutliersAbsThr': 250,  # [µV] maksymalna bezwzględna wartość amplitudy (liczona od 0)
    'OutliersStdThr': 7,  # wartość amplitudy, jako wielokrotność std
    'OutliersMedThr': 13,  # wartość amplitudy, jako wielokrotność mediany
    
    'MusclesAbsThr': np.inf,  # [µV²] maksymalna bezwzględna wartość średniej mocy na próbkę w zakresie częstości MusclesFreqRange
    'MusclesStdThr': 7,  # wartość amplitudy, jako wielokrotność std
    'MusclesMedThr': 5,  # wartość amplitudy, jako wielokrotność mediany
    
    'NoSigAbsThr': 1e-1,  # [µV] minimalna bezwzględna wartość amplitudy peak2peak w oknie NoSigWindow
}

PossibleChannels = ['Fp1', 'Fpz', 'Fp2', 'F7', 'F3', 'Fz', 'F4', 'F8', 'T3', 'C3',
                    'Cz', 'C4', 'T4', 'T5', 'P3', 'Pz', 'P4', 'T6', 'O1', 'Oz', 'O2',
                    'M1', 'M2', 'T7', 'T8', 'P7', 'P8']

artifact_labels = ["arbitrary", "median", "std"]


def getDefaultArtRejDict():
    return DefaultArtRejDict


def artifact_detection(rm, ch_2be_tested, art_detection_kargs = {}, outpath = ""):
    all_channels = rm.get_param('channels_names')
    
    # kanały, które będą testowane:
    ch_2be_tested = [c for c in ch_2be_tested if c in all_channels]
    
    # filtrowanie dla zwykłych artefaktów:
    filters = ([1., 0.5, 3, 6.97, "butter"],
               [30, 60, 3, 12.4, "butter"],
               [[47.5, 52.5], [49.9, 50.1], 3, 25, "cheby2"])
    
    # filtrowanie dla detekcji mięśni:
    line_fs = [50.]
    filters_muscle = []
    for line_f in line_fs:
        filters_muscle.append([[line_f - 2.5, line_f + 2.5], [line_f - 0.1, line_f + 0.1], 3.0, 25., "cheby2"])
    filters_muscle = tuple(filters_muscle)
    
    # jeśli którys z poniższych parametrów nie został podany ręcznie, to jest używana wartość domyślna
    forget = art_detection_kargs.get('forget', 0.)  # [s] długość fragmentów na początku i końcu sygnału, dla których nie zapisujemy tagów
    
    slope_window = art_detection_kargs.get('SlopeWindow', DefaultArtRejDict['SlopeWindow'])
    slow_window = art_detection_kargs.get('SlowWindow', DefaultArtRejDict['SlowWindow'])
    no_sig_window = art_detection_kargs.get('NoSigWindow', DefaultArtRejDict['NoSigWindow'])
    outliers_merge_win = art_detection_kargs.get('OutliersMergeWin', DefaultArtRejDict['OutliersMergeWin'])
    muscles_freq_range = art_detection_kargs.get('MusclesFreqRange', DefaultArtRejDict['MusclesFreqRange'])
    
    # progi powyżej których próbka jest oznaczana, jako slope:
    slopes_abs_thr = art_detection_kargs.get('SlopesAbsThr', DefaultArtRejDict['SlopesAbsThr'])
    slopes_std_thr = art_detection_kargs.get('SlopesStdThr', DefaultArtRejDict['SlopesStdThr'])
    slopes_med_thr = art_detection_kargs.get('SlopesMedThr', DefaultArtRejDict['SlopesMedThr'])
    
    # progi powyżej których próbka jest oznaczana, jako slow (fala wolna):
    slows_abs_thr = art_detection_kargs.get('SlowsAbsThr', DefaultArtRejDict['SlowsAbsThr'])
    slows_std_thr = art_detection_kargs.get('SlowsStdThr', DefaultArtRejDict['SlowsStdThr'])
    slows_med_thr = art_detection_kargs.get('SlowsMedThr', DefaultArtRejDict['SlowsMedThr'])
    
    # progi powyżej których próbka jest oznaczana, jako outlier:
    outliers_abs_thr = art_detection_kargs.get('OutliersAbsThr', DefaultArtRejDict['OutliersAbsThr'])
    outliers_std_thr = art_detection_kargs.get('OutliersStdThr', DefaultArtRejDict['OutliersStdThr'])
    outliers_med_thr = art_detection_kargs.get('OutliersMedThr', DefaultArtRejDict['OutliersMedThr'])
    
    # progi powyżej których próbka jest oznaczana, jako muscle:
    muscles_abs_thr = art_detection_kargs.get('MusclesAbsThr', DefaultArtRejDict['MusclesAbsThr'])
    muscles_std_thr = art_detection_kargs.get('MusclesStdThr', DefaultArtRejDict['MusclesStdThr'])
    muscles_med_thr = art_detection_kargs.get('MusclesMedThr', DefaultArtRejDict['MusclesMedThr'])
    
    # prog poniżej których próbka jest oznaczana, jako nosig (brak sygnału):
    no_sig_abs_thr = art_detection_kargs.get('NoSigAbsThr', DefaultArtRejDict['NoSigAbsThr'])
    
    fs = float(rm.get_param('sampling_frequency'))
    
    tags = []
    
    bad_samples, thresholds = outliers(rm, ch_2be_tested, filters, std_thr = outliers_std_thr, abs_thr = outliers_abs_thr, med_thr = outliers_med_thr,
                                       outpath = outpath)  # zaznaczanie outliers
    bad_samples = forget_birth_and_death(bad_samples, fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    tags.extend(art_tags_writer(bad_samples, fs, all_channels, merge_win = outliers_merge_win, art_name = "outlier", threshold_spec = thresholds))

    bad_samples, thresholds = peak2peak(rm, ch_2be_tested, slow_window, filters, std_thr = slows_std_thr, abs_thr = slows_abs_thr, med_thr = slows_med_thr,
                                        kind = 'slows', outpath = outpath)  # zaznaczanie fal wolnych
    bad_samples = forget_birth_and_death(bad_samples, fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    tags.extend(art_tags_writer(bad_samples, fs, all_channels, merge_win = slow_window, art_name = "slow", threshold_spec = thresholds))

    bad_samples, thresholds = peak2peak(rm, ch_2be_tested, slope_window, filters, std_thr = slopes_std_thr, abs_thr = slopes_abs_thr, med_thr = slopes_med_thr,
                                        kind = 'slopes', outpath = outpath)  # zaznaczanie iglic i stromych zboczy
    bad_samples = forget_birth_and_death(bad_samples, fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    tags.extend(art_tags_writer(bad_samples, fs, all_channels, merge_win = slope_window, art_name = "slope", threshold_spec = thresholds))

    bad_samples, thresholds = muscles(rm, ch_2be_tested, slow_window, muscles_freq_range, filters_muscle, std_thr = muscles_std_thr, abs_thr = muscles_abs_thr,
                                      med_thr = muscles_med_thr, outpath = outpath)  # zaznaczanie mięśni
    bad_samples = forget_birth_and_death(bad_samples, fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    tags.extend(art_tags_writer(bad_samples, fs, all_channels, merge_win = slow_window, art_name = "muscle", threshold_spec = thresholds))
    
    bad_samples, thresholds = nosignal(rm, ch_2be_tested, no_sig_window, filters, threshold = no_sig_abs_thr, outpath = outpath)
    bad_samples = forget_birth_and_death(bad_samples, fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    tags.extend(art_tags_writer(bad_samples, fs, all_channels, merge_win = no_sig_window, art_name = "nosig", threshold_spec = thresholds))
    
    return tags


def forget_birth_and_death(bs, fs, forget = 1.):
    for ch in bs:
        bs[ch][:int(fs * forget)] = 0.0
        if forget > 0:
            bs[ch][-int(fs * forget):] = 0.0
    return bs


def outliers(rm, channels, filters = tuple(), std_thr = np.inf, abs_thr = np.inf, med_thr = np.inf, full_output = False, outpath = ""):
    # w gruncie rzeczy to jeszcze można zrobić test, czy rozkład jest gaussowski,
    # bo czasami nie jest i wtedy można sugerować kanał, jako wadliwy
    # może to m.in. sugerować, że właściwości statystyczne kanału nie są stałe w czasie, np. odkleił się
    if outpath:
        fig, axs = plt.subplots(5, 5, figsize = (1. * 5 * figure_scale, .5625 * 5 * figure_scale))
        for ch in channels_not2draw(channels):
            pos = map1020[ch]
            axs[pos.y, pos.x].axis("off")
        fig.subplots_adjust(left = 0.03, bottom = 0.03, right = 0.98, top = 0.95, wspace = 0.16, hspace = 0.28)

    rm = deepcopy(rm)
    for filt in filters:
        print("Filtering...", filt)
        rm = mgr_filter(rm, filt[0], filt[1], filt[2],
                        filt[3], ftype = filt[4], use_filtfilt = True)
    
    bad_samples = dict()
    thresholds_dict = dict()
    all_thresholds_dict = dict()
    
    for ch in channels:
        sig = get_microvolt_samples(rm, ch)
        sig -= sig.mean()
        
        # obliczenie progu dla std
        cleaned_sig = sig.copy()
        s = np.inf
        while cleaned_sig.std() < s:
            s = cleaned_sig.std()
            cleaned_sig = cleaned_sig[np.abs(cleaned_sig) < std_thr * s + np.median(cleaned_sig)]

        sig_abs = np.abs(cleaned_sig)
        thresholds = np.array((abs_thr, med_thr * np.median(sig_abs), std_thr * s + np.median(sig_abs)))
        
        threshold = thresholds.min()  # wybieramy najostrzejszy (najniższy) próg
        threshold_type = ['abs', 'med', 'std'][thresholds.argmin()]
        
        print("{:>4}(outliers) - abs: {:.3g}, med: {:.3g}, std: {:.3g}, chosen: {}".format(*(ch,) + tuple(thresholds.tolist()) + (threshold_type,)))
        if threshold <= 0:
            raise Exception("Threshold = {}\nYou must give some threshold in range (0,+oo)!".format(threshold))
        
        thresholds_dict[ch] = (threshold, threshold_type)
        all_thresholds_dict[ch] = thresholds
        sig_abs = np.abs(sig)
        bad_samples[ch] = (sig_abs > threshold) * sig_abs

        if outpath:
            bins = max(20, sig_abs.size // 1000)
            pos = map1020[ch]
            ax = axs[pos.y, pos.x]
            sig_abs[sig_abs > 3*threshold] = 3*threshold  # trzeba przyciąć takie przypadki, bo potrafią rozwalić cały histogram

            ax.hist(sig_abs, bins = bins)
            mx = ax.get_ylim()[1]
            for i in range(3):
                if thresholds[i] < np.inf:
                    ax.plot([thresholds[i], thresholds[i]], [0, mx], label = artifact_labels[i])
            ax.plot([threshold], [mx], "o", label = threshold_type)
            ax.set_title("{} {}".format("outliers", ch))
            set_axis_fontsize(ax, fontsize * figure_scale / 8)
            ax.legend(fontsize = fontsize * figure_scale / 8)

    if outpath:
        print("saving figures...", outpath)
        fig.savefig(outpath.strip(".obci.tag")+"_outliers.png")
        plt.close(fig)

    if full_output:
        return bad_samples, all_thresholds_dict
    else:
        return bad_samples, thresholds_dict


def peak2peak(rm, channels, width, filters = tuple(), std_thr = np.inf, abs_thr = np.inf, med_thr = np.inf, kind = 'P2P', full_output = False, outpath = ""):
    if outpath:
        fig, axs = plt.subplots(5, 5, figsize = (1. * 5 * figure_scale, .5625 * 5 * figure_scale))
        for ch in channels_not2draw(channels):
            pos = map1020[ch]
            axs[pos.y, pos.x].axis("off")
        fig.subplots_adjust(left = 0.03, bottom = 0.03, right = 0.98, top = 0.95, wspace = 0.16, hspace = 0.28)
    
    rm = deepcopy(rm)
    for filt in filters:
        print("Filtering...", filt)
        rm = mgr_filter(rm, filt[0], filt[1], filt[2],
                        filt[3], ftype = filt[4], use_filtfilt = True)
    
    fs = int(float(rm.get_param('sampling_frequency')))
    width = int(width * fs)
    bad_samples = dict()
    thresholds_dict = dict()
    all_thresholds_dict = dict()
    for ch in channels:
        sig = get_microvolt_samples(rm, ch)
        
        mm_1 = minimax(sig, width)
        # przesuniecie o polowe szerokosci okna i ponowne obliczenie
        mm_2 = minimax(np.roll(sig, -int(width // 2)), width)
        
        # obliczenie progu dla std
        mm_mm = np.concatenate([mm_1, mm_2])
        if outpath:
            mm_for_hist = mm_mm.copy()
        s = np.inf
        while mm_mm.std() < s:
            s = mm_mm.std()
            mm_mm = mm_mm[mm_mm < std_thr * s + np.median(mm_mm)]
        
        thresholds = np.array((abs_thr, med_thr * np.median(mm_mm), std_thr * s + np.median(mm_mm)))
        threshold = thresholds.min()  # wybieramy najostrzejszy (najnizszy) prog
        threshold_type = ['abs', 'med', 'std'][thresholds.argmin()]
        print("{:>4}({}) - abs: {:.3g}, med: {:.3g}, std: {:.3g}, chosen: {}".format(*(ch, kind) + tuple(thresholds.tolist()) + (threshold_type,)))
        
        if threshold <= 0:
            raise Exception("Threshold = {}\nYou must give some threshold in range (0,+oo)!".format(threshold))
        bad_segment_1 = mm_1 * (mm_1 > threshold)  # jednostronnie, bo interesują nas tylko przypadki o zbyt dużej, a nie zbyt małej amplitudzie
        bad_segment_2 = mm_2 * (mm_2 > threshold)  # jednostronnie przesunięte
        
        thresholds_dict[ch] = (threshold, threshold_type)
        all_thresholds_dict[ch] = thresholds
        bad_samples[ch] = np.max([badseg2badsamp(bad_segment_1, width, sig.size), badseg2badsamp(bad_segment_2, width, sig.size, int(width // 2))], axis = 0)
        
        if DEBUG and kind == "slows" and ch == "Fp1":
            timeline = np.arange(sig.size, dtype = float)/DEBUG_FS
            right = 0
            while np.argmax(bad_samples[ch][right:]) > 0:
                left = right + np.argmax(bad_samples[ch][right:].astype(bool))
                right = left + bad_samples[ch][left:].argmin()
                print("{} {}".format(ch, timeline[left]))
                plt.figure()
                plt.title("{} {}".format(ch, timeline[left]))
                plt.plot(timeline[left-10:right+10], sig[left-10:right+10])
                plt.plot(timeline[left-10:right+10], bad_samples[ch][left-10:right+10])
                # plt.show()

        if outpath:
            bins = max(20, mm_for_hist.size//100)
            pos = map1020[ch]
            ax = axs[pos.y, pos.x]
            mm_for_hist[mm_for_hist > 3*threshold] = 3*threshold  # trzeba przyciąć takie przypadki, bo potrafią rozwalić cały histogram
            ax.hist(mm_for_hist, bins = bins)
            mx = ax.get_ylim()[1]
            for i in range(3):
                if thresholds[i] < np.inf:
                    ax.plot([thresholds[i], thresholds[i]], [0, mx], label = artifact_labels[i])
            ax.plot([threshold], [mx], "o", label = threshold_type)
            ax.set_title("{} {}".format(kind, ch))
            set_axis_fontsize(ax, fontsize * figure_scale / 8)
            ax.legend(fontsize = fontsize * figure_scale / 8)
 
    if outpath:
        fig.savefig(outpath.strip(".obci.tag")+"_{}.png".format(kind))
        plt.close(fig)
 
    # plt.plot(c[bad_samples[ch] > 0])
    # plt.plot(bad_samples[ch][bad_samples[ch] > 0], 'r')
    # plt.title(ch)
    # plt.show()

    if full_output:
        return bad_samples, all_thresholds_dict
    else:
        return bad_samples, thresholds_dict


def muscles(rm, channels, width, rng, filters = tuple(), std_thr = np.inf, abs_thr = np.inf, med_thr = np.inf, full_output = False, outpath = ""):
    if outpath:
        fig, axs = plt.subplots(5, 5, figsize = (1. * 5 * figure_scale, .5625 * 5 * figure_scale))
        for ch in channels_not2draw(channels):
            pos = map1020[ch]
            axs[pos.y, pos.x].axis("off")
        fig.subplots_adjust(left = 0.03, bottom = 0.03, right = 0.98, top = 0.95, wspace = 0.16, hspace = 0.28)
    
    rm = deepcopy(rm)
    for filt in filters:
        print("Filtering...", filt)
        rm = mgr_filter(rm, filt[0], filt[1], filt[2],
                        filt[3], ftype = filt[4], use_filtfilt = True)
    
    fs = int(float(rm.get_param('sampling_frequency')))
    width = int(width * fs)
    bad_samples = dict()
    thresholds_dict = dict()
    all_thresholds_dict = dict()
    for ch in channels:
        sig = get_microvolt_samples(rm, ch)
        sig -= sig.mean()
        
        p_msc_1, freqs = sig_power(sig, width, rng, fs)
        # przesunięcie o połowę szerokości okna i ponowne obliczenie
        p_msc_2, freqs = sig_power(np.roll(sig, -int(width // 2)), width, rng, fs)
        
        # obliczenie progu dla std
        pp_msc = np.concatenate([p_msc_1, p_msc_2])
        if outpath:
            pp_for_hist = pp_msc.copy()
        s = np.inf
        while pp_msc.std() < s:
            s = pp_msc.std()
            pp_msc = pp_msc[pp_msc < std_thr * s + np.median(pp_msc)]
        
        thresholds = np.array((abs_thr, med_thr * np.median(pp_msc), std_thr * s + np.median(pp_msc)))
        threshold = thresholds.min()  # wybieramy najostrzejszy (najnizszy) prog
        threshold_type = ['abs', 'med', 'std'][thresholds.argmin()]
        print("{:>4}(muscles) - abs: {:.3g}, med: {:.3g}, std: {:.3g}, chosen: {}".format(*(ch,) + tuple(thresholds.tolist()) + (threshold_type,)))
        
        if threshold <= 0:
            raise Exception("Threshold = {}\nYou must give some threshold in range (0,+oo)!".format(threshold))
        
        bad_segment_1 = p_msc_1 * (p_msc_1 > threshold)  # jednostronnie, bo interesują nas tylko przypadki o zbyt dużej, a nie zbyt małej mocy
        bad_segment_2 = p_msc_2 * (p_msc_2 > threshold)  # jednostronnie
        
        thresholds_dict[ch] = (threshold, threshold_type)
        all_thresholds_dict[ch] = thresholds
        bad_samples[ch] = np.max([badseg2badsamp(bad_segment_1, width, sig.size), badseg2badsamp(bad_segment_2, width, sig.size, int(width // 2))], axis = 0)

        if outpath:
            bins = max(20, pp_for_hist.size // 100)
            pos = map1020[ch]
            ax = axs[pos.y, pos.x]
            pp_for_hist[pp_for_hist > 3*threshold] = 3*threshold  # trzeba przyciąć takie przypadki, bo potrafią rozwalić cały histogram
            ax.hist(pp_for_hist, bins = bins)
            mx = ax.get_ylim()[1]
            for i in range(3):
                if thresholds[i] < np.inf:
                    ax.plot([thresholds[i], thresholds[i]], [0, mx], label = artifact_labels[i])
            ax.plot([threshold], [mx], "o", label = threshold_type)
            ax.set_title("{} {}".format("muscles", ch))
            set_axis_fontsize(ax, fontsize * figure_scale / 8)
            ax.legend(fontsize = fontsize * figure_scale / 8)

    if outpath:
        fig.savefig(outpath.strip(".obci.tag")+"_muscles.png")
        plt.close(fig)
    
    if full_output:
        return bad_samples, all_thresholds_dict
    else:
        return bad_samples, thresholds_dict


def nosignal(rm, channels, width, filters = tuple(), threshold = 0, outpath = ""):
    if outpath:
        fig, axs = plt.subplots(5, 5, figsize = (1. * 5 * figure_scale, .5625 * 5 * figure_scale))
        for ch in channels_not2draw(channels):
            pos = map1020[ch]
            axs[pos.y, pos.x].axis("off")
        fig.subplots_adjust(left = 0.03, bottom = 0.03, right = 0.98, top = 0.95, wspace = 0.16, hspace = 0.28)
    
    rm = deepcopy(rm)
    for filt in filters:
        print("Filtering...", filt)
        rm = mgr_filter(rm, filt[0], filt[1], filt[2],
                        filt[3], ftype = filt[4], use_filtfilt = True)
    
    fs = int(float(rm.get_param('sampling_frequency')))
    width = int(width * fs)
    bad_samples = dict()
    threshold_dict = dict()
    for ch in channels:
        c = get_microvolt_samples(rm, ch)
        
        mm_1 = minimax(c, width)
        # przesuniecie o polowe szerokosci okna i ponowne obliczenie
        mm_2 = minimax(np.roll(c, -int(width // 2)), width)

        if outpath:
            mm_for_hist = np.concatenate([mm_1, mm_2])
            
        threshold_type = 'abs'
        print("{:>4}(nosig) - abs: {:.3g}".format(ch, threshold))
        
        if threshold <= 0:
            raise Exception("Threshold = {}\nYou must give some threshold in range (0,+oo)!".format(threshold))
        bad_segment_1 = mm_1 * (mm_1 < threshold)  # jednostronnie, bo interesują nas tylko przypadki o zbyt małej, a nie zbyt dużej amplitudzie
        bad_segment_2 = mm_2 * (mm_2 < threshold)  # jednostronnie przesunięte
        
        threshold_dict[ch] = (threshold, threshold_type)
        bad_samples[ch] = np.max([badseg2badsamp(bad_segment_1, width, c.size), badseg2badsamp(bad_segment_2, width, c.size, int(width // 2))], axis = 0)
        
        if DEBUG and ch == "Fz":
            timeline = np.arange(c.size, dtype = float) / DEBUG_FS
            right = 0
            while np.argmax(bad_samples[ch][right:]) > 0:
                left = right + np.argmax(bad_samples[ch][right:].astype(bool))
                right = left + bad_samples[ch][left:].argmin()
                print("{} {}".format(ch, timeline[left]))
                plt.figure()
                plt.title("{} {}".format(ch, timeline[left]))
                plt.plot(timeline[left - 10:right + 10], c[left - 10:right + 10])
                plt.plot(timeline[left - 10:right + 10], bad_samples[ch][left - 10:right + 10])
                # plt.show()
        
        if outpath:
            bins = max(20, mm_for_hist.size // 100)
            pos = map1020[ch]
            ax = axs[pos.y, pos.x]
            hist_limit = 5 * np.median(mm_for_hist)
            mm_for_hist[mm_for_hist > hist_limit] = hist_limit  # trzeba przyciąć takie przypadki, bo potrafią rozwalić cały histogram
            ax.hist(mm_for_hist, bins = bins)
            mx = ax.get_ylim()[1]
            if threshold > 0:
                    ax.plot([threshold, threshold], [0, mx], label = "abs")
            ax.set_title("nosig {}".format(ch))
            set_axis_fontsize(ax, fontsize * figure_scale / 8)
            ax.legend(fontsize = fontsize * figure_scale / 8)
    
    if outpath:
        fig.savefig(outpath.strip(".obci.tag") + "_nosig.png")
        plt.close(fig)
    
    # plt.plot(c[bad_samples[ch] > 0])
    # plt.plot(bad_samples[ch][bad_samples[ch] > 0], 'r')
    # plt.title(ch)
    # plt.show()
    
    return bad_samples, threshold_dict


def badseg2badsamp(bad_segments, width, size, shift = 0):
    y = np.zeros(size, dtype = bad_segments.dtype)
    width = int(width)
    shift = int(shift)
    for i in range(bad_segments.size):
        y[i * width + shift:(i + 1) * width + shift] = bad_segments[i]
    return y


def art_tags_writer(bad_samples, fs, all_channels, merge_win, art_name = 'artifact', threshold_spec = np.inf, min_duration = 0.03):
    tags = []
    for ch in bad_samples:
        if threshold_spec == np.inf:
            threshold, threshold_type = np.inf, 'art'  # default
        else:
            threshold, threshold_type = threshold_spec[ch]
        
        bads = bad_samples[ch]
        start = stop = 0.
        artifact = False
        value = -np.inf
        for i in range(bads.size):
            if bads[i]:
                if bads[i] > value:
                    value = bads[i]
                stop = i
                if not artifact:
                    start = i
                    artifact = True
            elif artifact:
                if i - stop > merge_win * fs:
                    if stop - start < int(min_duration * fs):  # symetryczne rozszerzenie tagu, żeby nie był węższy niż MinDuration
                        Ext = int(min_duration * fs) - stop + start
                        stop += Ext / 2
                        start -= Ext / 2
                    try:
                        all_channels.index(ch)
                    except ValueError:
                        print(all_channels)
                        print(ch)
                        raise
                    tag = {'channelNumber': all_channels.index(ch), 'start_timestamp': 1. * start / fs, 'name': art_name, 'end_timestamp': 1. * stop / fs,
                           'desc': {'thr': "{}".format(int(threshold)), 'val': "{}".format(int(value)), 'typ': threshold_type}}
                    tags.append(tag)
                    value = -np.inf
                    artifact = False
                    
                    if DEBUG:
                        timeline = 1. * np.arange(bads.size) / fs
                        if timeline[start] > 2 and tag['name'] == "slow" and ch == "Fp1":
                            plt.figure()
                            plt.plot(timeline[start:stop], bads[start:stop])
                            plt.title("{} {}".format(ch, tag['start_timestamp']))

    if DEBUG:
        plt.show()

    return tags


def art_tags_saver(artifacts_file_path, tags):
    try:
        os.makedirs(os.path.dirname(artifacts_file_path))
    except OSError:
        pass
    
    writer = TagsFileWriter(artifacts_file_path)
    for tag in tags:
        writer.tag_received(tag)
    writer.finish_saving(0.0)


def minimax(s, w):
    mm = []
    for i in range(0, s.size, w):
        m = s[i:i + w].max() - s[i:i + w].min()
        mm.append(m)
    return np.array(mm)


def sig_power(s, w, rng, fs):
    ps = []
    freq = np.fft.rfftfreq(w, 1. / fs)
    a = np.argmin(np.abs(rng[0] - freq))
    b = np.argmin(np.abs(rng[1] - freq))
    if a == b:
        print("UWAGA! W podanym zakresie {} znalazła się tylko jedna częstość: {}".format(rng, freq[a]))
    
    window = signal.hanning(w)
    for i in range(0, s.size // w * w, w):
        f = np.fft.rfft(window * s[i:i + w], norm = "ortho")
        p = np.mean(np.abs(f[a:b + 1]) ** 2)
        ps.append(p)
    
    ps = np.array(ps)
    return ps, (freq[a], freq[b])


if __name__ == "__main__":
    import getpass
    from .obci.analysis.obci_signal_processing import read_manager
    from .helper_functions import montage_ears
    
    path_prefix = ''
    if sys.argv[1:]:
        datapaths = sys.argv[1:]
    else:
        if getpass.getuser() == 'marcin':
            path_prefix = '/run/user/1000/gvfs/sftp:host=gabor.zfb.fuw.edu.pl,user=mpietrzak'
            if not os.path.exists(path_prefix):
                path_prefix = "/media/dane/Dokumenty/repo_mirror"
        else:
            path_prefix = ''
        
        datapaths = ["/repo/coma/dane_pomiarowe_dzienne"]

    selected_channels = [u'Fp1', u'Fp2', u'AFz', u'F7', u'F3', u'Fz', u'F4', u'F8', u'T7', u'C3', u'Cz',
                         u'C4', u'T8', u'P7', u'P3', u'Pz', u'P4', u'P8', u'O1', u'O2', u'T3', u'T4', u'T5', u'T6']

    for datapath in datapaths:
        for root, dirs, files in os.walk(path_prefix+datapath):
            for filename in files:  # przeglądamy rekursywnie wszystkie pliki w katalogu
                # sprawdzamy, czy sciezka do pliku i nazwa pliku pasuje do wzoru:
                if "global-local" not in root or "control" in root or "-bak" in root or ".raw" not in filename or ".etr." in filename:
                    continue
                print(filename)
                path_to_raw = os.path.join(root, filename)

                print(path_to_raw)
                core_of_path = path_to_raw[:-4]

                print("reading file...")
                eeg_rm = read_manager.ReadManager(core_of_path + '.xml', core_of_path + '.raw', core_of_path + '.tag')
                channels_names = eeg_rm.get_param('channels_names')

                print("preprocessing...")
                fs = float(eeg_rm.get_param('sampling_frequency'))

                eeg_rm.set_samples(get_microvolt_samples(eeg_rm), eeg_rm.get_param('channels_names'))
                eeg_rm.set_param('channels_gains', np.ones(len(channels_names)))

                drop_chnls = [u'EOGL', u'EOGR', u'EMG', u'EKG', u'RESP', u'RESP_UP', u'RESP_U', u'RESP_DOWN', u'RESP_D', u'TERM', u'SaO2', u'Pleth', u'HRate', u'Saw', u'Driver_Saw', u'Sample_Counter']
                try:
                    x_ref = montage_ears(eeg_rm, 'A1', 'A2', exclude_from_montage = drop_chnls)
                except ValueError:
                    try:
                        x_ref = montage_ears(eeg_rm, 'M1', 'M2', exclude_from_montage = drop_chnls)
                    except ValueError:
                        print("WARNING! Cannot mount to ears for file {}".format(filename))
                        continue
                eeg_rm = x_ref

                available_channels = eeg_rm.get_param('channels_names')
                
                print("marking artifacts...")
                art_tagfile_path = core_of_path.rstrip(".obci") + '_artifacts.obci.tag'
                
                tags = artifact_detection(eeg_rm, selected_channels, outpath = art_tagfile_path)
                
                print('tagfile_path:', art_tagfile_path)
                art_tags_saver(art_tagfile_path, tags)
