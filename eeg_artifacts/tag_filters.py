import six


def create_custom_filter(key, values, key2 = None):
    """ Function creating custom tag filter - to return tags fulfilling requirements

    :param key: name of tag feature
    :param values: list of allowed values of given tag feature; can also be a single string
    :param key2: second key - if first key is 'desc' (description)
    :return: True if requirment is fulfilled, False in other case or if it cannot be evaluated
    """

    if isinstance(values, six.string_types):
        values = (values,)
    
    if key2:
        def func(tag):
            try:
                return tag[key][key2] in values
            except KeyError:
                return False
    else:
        def func(tag):
            try:
                return tag[key] in values
            except KeyError:
                return False
    
    return func


# a few most common filters:

def target(tag):
    try:
        return tag['desc']['type'] == 'target'
    except KeyError:
        return False


def nontarget(tag):
    try:
        return tag['desc']['type'] == 'nontarget'
    except KeyError:
        return False


def dewiant(tag):
    try:
        return tag['desc']['type_local'] == 'dewiant'
    except KeyError:
        return False


def standard(tag):
    try:
        return tag['desc']['type_local'] == 'standard'
    except KeyError:
        return False


def reka(tag):
    try:
        return tag['name'] == 'ERDS_instr1.wav'
    except KeyError:
        return False


def noga(tag):
    try:
        return tag['name'] == 'ERDS_instr2.wav'
    except KeyError:
        return False

