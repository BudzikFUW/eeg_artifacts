#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import six
import numpy as np
from scipy import signal
from .obci.analysis.obci_signal_processing.signal import read_data_source
from .obci.analysis.obci_signal_processing import read_manager
from .obci.analysis.obci_signal_processing.smart_tags_manager import SmartTagsManager
from .obci.analysis.obci_signal_processing.tags.smart_tag_definition import SmartTagDurationDefinition

from collections import namedtuple
from copy import deepcopy

fontsize = 21
figure_scale = 8


Pos = namedtuple('Pos', ['x', 'y'])
map1020 = {'eog': Pos(0, 0), 'Fp1': Pos(1, 0), 'Fpz': Pos(2, 0), 'Fp2': Pos(3, 0), 'Null': Pos(4, 0),
           'F7': Pos(0, 1), 'F3': Pos(1, 1), 'Fz': Pos(2, 1), 'F4': Pos(3, 1), 'F8': Pos(4, 1),
           'T3': Pos(0, 2), 'C3': Pos(1, 2), 'Cz': Pos(2, 2), 'C4': Pos(3, 2), 'T4': Pos(4, 2),
           'T5': Pos(0, 3), 'P3': Pos(1, 3), 'Pz': Pos(2, 3), 'P4': Pos(3, 3), 'T6': Pos(4, 3),
           'M1': Pos(0, 4), 'O1': Pos(1, 4), 'Oz': Pos(2, 4), 'O2': Pos(3, 4), 'M2': Pos(4, 4)}


def channels_not2draw(channels):
    """zwraca listę kanałów, których brakuje w liście channels, a są w słowniku map1020"""
    ch_not2draw = []
    for ch in map1020:
        if ch not in channels:
            ch_not2draw.append(ch)
    return ch_not2draw


def set_axis_fontsize(ax, size):
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                     ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(size)


def get_microvolt_samples(rm, channels = None, startpoint = None, endpoint = None, length = None):
    """Gets microvolt eeg signal from read manager or smart tags
    from startpoint to endpoint or of given lentgth
    for specified set of channels.
    
    I.e. does get_samples on smart tag (read manager), but multiplied by channel gain
    and in case of taking only part of signal, it defines its start, end or length in seconds not in samples.

    :param rm: smart tag or read manager object
    :param channels: channel names or list of channel names for which return microvolt samples, default returns for all
    :param startpoint: starting point [s] of returned part of signal
    :param endpoint: end point [s] of returned part of section
    :param length: length [s] of returned signal part (overrides endpoint if provided)
    :return: Multi- or single- channel eeg signal in microvolts
    """
    total_len = int(rm.get_param("number_of_samples"))
    fs = float(rm.get_param('sampling_frequency'))
    if startpoint:
        p_from = int(fs * startpoint)
    else:
        p_from = 0

    if length:
        p_len = int(fs * length)
    elif endpoint:
        p_len = int(fs * endpoint) - p_from
    else:
        p_len = total_len

    if p_from < 0:
        p_from = 0
    if p_from + p_len > total_len:
        p_len = total_len - p_from

    if not channels:  # returns for all channels
        gains = np.array([float(i) for i in rm.get_param('channels_gains')], ndmin = 2).T
        return rm.get_samples(p_from = p_from, p_len = p_len) * gains
    elif isinstance(channels, six.string_types):  # returns for specific channel
        channel = channels
        ch_n = rm.get_param('channels_names').index(channel)
        gain = rm.get_param('channels_gains')[ch_n]
        return rm.get_channel_samples(channel, p_from = p_from, p_len = p_len) * float(gain)
    else:  # returns for specified list of channels
        gains = rm.get_param('channels_gains')
        gains = np.array([float(gains[channels.index(ch)]) for ch in channels], ndmin = 2).T
        return rm.get_channels_samples(channels, p_from = p_from, p_len = p_len) * gains


def mgr_filter(mgr, wp, ws, gpass, gstop, analog = 0, ftype = 'ellip', output = 'ba', unit = 'hz', use_filtfilt = True, meancorr = 1.0):
    if unit == 'radians':
        b, a = signal.iirdesign(wp, ws, gpass, gstop, analog, ftype, output)
    elif unit == 'hz':
        nyquist = float(mgr.get_param('sampling_frequency')) / 2.0
        try:
            wp = wp / nyquist
            ws = ws / nyquist
        except TypeError:
            wp = [i / nyquist for i in wp]
            ws = [i / nyquist for i in ws]
        b, a = signal.iirdesign(wp, ws, gpass, gstop, analog, ftype, output)
    if use_filtfilt:
        for i in range(int(mgr.get_param('number_of_channels'))):
            mgr.get_samples()[i, :] = signal.filtfilt(b, a, mgr.get_samples()[i] - np.mean(mgr.get_samples()[i]) * meancorr)
        samples_source = read_data_source.MemoryDataSource(mgr.get_samples(), False)
    else:
        print("FILTER CHANNELs")
        filtered = signal.lfilter(b, a, mgr.get_samples())
        print("FILTER CHANNELs finished")
        samples_source = read_data_source.MemoryDataSource(filtered, True)
    info_source = deepcopy(mgr.info_source)
    tags_source = deepcopy(mgr.tags_source)
    new_mgr = read_manager.ReadManager(info_source, samples_source, tags_source)
    return new_mgr


def montage_csa(mgr, exclude_from_montage = []):
    exclude_from_montage_indexes = _exclude_from_montage_indexes(mgr, exclude_from_montage)
    new_samples = get_montage(mgr.get_samples(),
                              get_montage_matrix_csa(int(mgr.get_param('number_of_channels')),
                                                     exclude_from_montage = exclude_from_montage_indexes))
    info_source = deepcopy(mgr.info_source)
    tags_source = deepcopy(mgr.tags_source)
    samples_source = read_data_source.MemoryDataSource(new_samples)
    return read_manager.ReadManager(info_source, samples_source, tags_source)


def montage_ears(mgr, l_ear_channel, r_ear_channel, exclude_from_montage = []):
    try:
        left_index = mgr.get_param('channels_names').index(l_ear_channel)
    except ValueError:
        print("Brakuje kanału usznego {}. Wykonuję montaż tylko do jegnego ucha.".format(l_ear_channel))
        return montage_custom(mgr, [r_ear_channel], exclude_from_montage)
    try:
        right_index = mgr.get_param('channels_names').index(r_ear_channel)
    except ValueError:
        print("Brakuje kanału usznego {}. Wykonuję montaż tylko do jegnego ucha.".format(r_ear_channel))
        return montage_custom(mgr, [l_ear_channel], exclude_from_montage)
    
    exclude_from_montage_indexes = _exclude_from_montage_indexes(mgr, exclude_from_montage)
    
    if left_index < 0 or right_index < 0:
        raise Exception("Montage - couldn`t find ears channels: " + str(l_ear_channel) + ", " + str(r_ear_channel))
    
    new_samples = get_montage(mgr.get_samples(),
                              get_montage_matrix_ears(int(mgr.get_param('number_of_channels')),
                                                      left_index,
                                                      right_index,
                                                      exclude_from_montage_indexes)
                              )
    info_source = deepcopy(mgr.info_source)
    tags_source = deepcopy(mgr.tags_source)
    samples_source = read_data_source.MemoryDataSource(new_samples)
    return read_manager.ReadManager(info_source, samples_source, tags_source)


def montage_custom(mgr, chnls, exclude_from_montage = []):
    """apply custom montage to manager, by chnls"""
    
    exclude_from_montage_indexes = _exclude_from_montage_indexes(mgr, exclude_from_montage)
    
    indexes = []
    for chnl in chnls:
        print(mgr.get_param('channels_names'))
        index = mgr.get_param('channels_names').index(chnl)
        if index < 0:
            raise Exception("Montage - couldn`t channel: " + str(chnl))
        else:
            indexes.append(index)
    
    new_samples = get_montage(mgr.get_samples(),
                              get_montage_matrix_custom(int(mgr.get_param('number_of_channels')),
                                                        indexes,
                                                        exclude_from_montage = exclude_from_montage_indexes)
                              )
    info_source = deepcopy(mgr.info_source)
    tags_source = deepcopy(mgr.tags_source)
    samples_source = read_data_source.MemoryDataSource(new_samples)
    return read_manager.ReadManager(info_source, samples_source, tags_source)


def _exclude_from_montage_indexes(mgr, chnames):
    exclude_from_montage_indexes = []
    
    for i in chnames:
        try:
            exclude_from_montage_indexes.append(mgr.get_param('channels_names').index(i))
        except ValueError:
            pass
    return exclude_from_montage_indexes


def get_montage(data, montage_matrix):
    """
    montage_matrix[i] = linear transformation of all channels to achieve _new_ channel i
    data[i] = original data from channel i

    >>> montage_matrix = np.array([[ 1.  , -0.25, -0.25, -0.25, -0.25], [-0.25,  1.  , -0.25, -0.25, -0.25], [-0.25, -0.25,  1.  , -0.25, -0.25],[-0.25, -0.25, -0.25,  1.  , -0.25], [-0.25, -0.25, -0.25, -0.25,  1.  ]])
    >>> data = np.array(5 * [np.ones(10)])
    >>> montage(data,montage_matrix)
    array([[ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.]])


    """
    
    return np.dot(montage_matrix, data)


def get_montage_matrix_csa(n, exclude_from_montage = []):
    """
    Return nxn array representing extraction from
    every channel an avarage of all other channels.

    exclude_from_montage - list of indexes not to include in montage

    >>> get_montage_matrix_avg(5)
    array([[ 1.  , -0.25, -0.25, -0.25, -0.25],
           [-0.25,  1.  , -0.25, -0.25, -0.25],
           [-0.25, -0.25,  1.  , -0.25, -0.25],
           [-0.25, -0.25, -0.25,  1.  , -0.25],
           [-0.25, -0.25, -0.25, -0.25,  1.  ]])

    """
    
    n_fac = n - len(exclude_from_montage)
    factor = -1.0 / (n_fac - 1)
    mx = np.ones((n, n))
    for i in range(n):
        for j in range(n):
            if i != j and not i in exclude_from_montage:
                mx[i, j] = factor
    return mx


def get_montage_matrix_ears(n, l_ear_index, r_ear_index, exclude_from_montage = []):
    """
    Return nxn array representing extraction from
    every channel an avarage of channels l_ear_index
    and r_ear_index.

    exclude_from_montage - list of indexes not to include in montage

    >>> get_montage_matrix_ears(5, 2, 4)
    array([[ 1. ,  0. , -0.5,  0. , -0.5],
           [ 0. ,  1. , -0.5,  0. , -0.5],
               [ 0. ,  0. ,  1. ,  0. ,  0. ],
               [ 0. ,  0. , -0.5,  1. , -0.5],
               [ 0. ,  0. ,  0. ,  0. ,  1. ]])
    """
    
    factor = -0.5
    mx = np.diag([1.0] * n)
    for i in range(n):
        for j in range(n):
            if j in [r_ear_index, l_ear_index] and j != i and i not in [r_ear_index, l_ear_index] + exclude_from_montage:
                mx[i, j] = factor
    return mx


def get_montage_matrix_custom(n, indexes, exclude_from_montage = []):
    """
    Return nxn array representing extraction from
    every channel an avarage of channels in indexes list

    exclude_from_montage - list of indexes not to include in montage

    >>> get_montage_matrix_custom(5, [2, 4])
    array([[ 1. ,  0. , -0.5,  0. , -0.5],
           [ 0. ,  1. , -0.5,  0. , -0.5],
               [ 0. ,  0. ,  1. ,  0. ,  0. ],
               [ 0. ,  0. , -0.5,  1. , -0.5],
               [ 0. ,  0. ,  0. ,  0. ,  1. ]])
    """
    
    factor = -1.0 / len(indexes)
    mx = np.diag([1.0] * n)
    for i in range(n):
        for j in range(n):
            if j in indexes and j != i and i not in indexes + exclude_from_montage:
                mx[i, j] = factor
    return mx


def get_epochs_from_rm(rm, tags_function_list = None,
                       start_offset = -0.1, duration = 2.0,
                       tag_name = None):
    """Extracts stimulus epochs from ReadManager to list of SmartTags

    :param rm: ReadManager with dataset
    :param tags_function_list: list of tag filters to get epochs for, None or empty sequence will return epochs for all tags, default None
    :param start_offset: baseline in negative seconds
    :param duration: duration of the epoch (excluding baseline)
    :param tag_name: common name for filtered tags
    :return: list of SmartTags
    """
    if not tags_function_list:
        tags_function_list = [lambda x: True]
    
    # removing tags that do not have enough signal before to apply offset
    new_tags = [tag for tag in rm.get_tags() if float(tag['start_timestamp']) > -start_offset]
    rm.set_tags(new_tags)

    tag_def = SmartTagDurationDefinition(start_tag_name = tag_name,
                                         start_offset = start_offset,
                                         end_offset = 0.0,
                                         duration = duration)
    stags = SmartTagsManager(tag_def, '', '', '', p_read_manager = rm)
    returntags = []
    for tagfunction in tags_function_list:
        returntags += stags.get_smart_tags(p_func = tagfunction, )
    print('Found {} epochs for defined filters.'.format(len(returntags)))

    return returntags
