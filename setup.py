import setuptools

with open("README.md", "r") as fh:

    long_description = fh.read()

setuptools.setup(

     name='eeg_artifacts',

     version='0.1.1',

     author="Marcin Pietrzak",

     author_email="marcinptrzk@gmail.com",

     description="EEG artifacts marking tools based on BrainTech OpenBCI ReadManager interface",

     long_description=long_description,

     long_description_content_type="text/markdown",

     url="https://gitlab.com/BudzikFUW/eeg_artifacts",

     packages=setuptools.find_packages(),

     classifiers=[

         "Programming Language :: Python",

         "License :: OSI Approved :: GNU General Public License (GPL)",

         "Operating System :: Linux",
     ],
 )