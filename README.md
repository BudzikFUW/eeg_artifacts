# EEG Artifacts
##EEG artifacts marking tools based on OpenBCI-ReadManager interface

Code works both on Python 2 and Python 3.

Module development supported by NCN Opus Grant:

"Interfejs mózg-komputer do diagnozy i komunikacji w zaburzeniach świadomości" finansowanego przez Naukowego Centrum Nauki. Projek realizowany przez Wydział Fizyki Uniwersytetu Warszawskiego.
Czas realizacji projektu: 2016 - 2018 r.

# Twórcy
* Marcin Pietrzak (@marcinp)
* Marian Dovgialo (@mdov)
* Piotr Durka (supervisor)